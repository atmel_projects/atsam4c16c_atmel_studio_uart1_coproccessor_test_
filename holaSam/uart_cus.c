/*
 * uart_cus.c
 *
 * Created: 04/09/2017 09:20:52 a.m.
 *  Author: hydro
 */ 
#include "uart_cus.h"

void UART_Init(){
	//configure PIO controller A  - disable means enable peripheral on pins
	//disable PIOA control of PA9 and enable peripheral on pin
	REG_PIOB_PDR |= PIO_PDR_P4;
	//disable PIOA control of PA9 and enable peripheral on pin
	REG_PIOB_PDR |= PIO_PDR_P5;
	REG_PIOB_ABCDSR &=  ~(PIO_ABCDSR_P4);
	REG_PIOB_ABCDSR &=  ~(PIO_ABCDSR_P5);
	
	//configure PMC UART Clock
	//enable UART0 clock
	REG_PMC_PCER0 |= PMC_PCER0_PID8;
	
	//configure buad rate
	//REG_UART0_BRGR |= 130;
	
	//REG_UART0_BRGR |= 53;
	//fcpu/16xBR 8,000,000/(16x9600)
	
	//REG_UART0_BRGR |= 104;
	//fcpu/16xBR 16,000,000/(16x9600) PLLB
	
	REG_UART0_BRGR |= 24;
	//fcpu/16xBR 16,000,000/(16x115200) PLLB
	
	//parity
	REG_UART0_MR |= UART_MR_PAR_NO;
	
	//mode
	//normal mode default
	
	//enable transmit/receive
	REG_UART0_CR |= UART_CR_TXEN;
	REG_UART0_CR |= UART_CR_RXEN;
	
	//enable interrupt on receive
	REG_UART0_IER |= UART_IER_RXRDY;
}

void transmitByte(uint8_t data){
	//wait for ready
	while (!(REG_UART0_SR & UART_SR_TXRDY));
	while (!(REG_UART0_SR & UART_SR_TXEMPTY));
	REG_UART0_THR |= data;
}

void printString(const char myString[]) {
	uint8_t i = 0;
	while (myString[i]) {
		transmitByte(myString[i]);
		i++;
	}
}

void UART0_Handler(void) {
	// when we receive a byte, transmit that byte back
	uint32_t status = REG_UART0_SR;
	if ((status & UART_SR_RXRDY)){
		//read receive holding register
		uint8_t readByte = REG_UART0_RHR;
		//transmit that byte back
		transmitByte(readByte);
	}
}