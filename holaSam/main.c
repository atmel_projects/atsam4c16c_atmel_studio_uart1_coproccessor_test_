#include "sam.h"
#include "stdio.h"
#include "uart_cus.h"
#include "clock_setup.h"

void config()
{
	SystemInit();
	clock_init();
	REG_WDT_MR = WDT_MR_WDDIS;
	UART_Init();
	NVIC_EnableIRQ(UART0_IRQn);
}


int main(void)
{
	/* Initialize the SAM system */
	config();	
	
	char buffer[100];
	float c = 3.5;
	//sprintf(buffer, "%f", c);
	char cad[] = "\"Voltage\"\:127.34 \"Current\"\:17.22 \"PF\"\:23.12 \"Consumption\":231.34 ";

	//printString("\"Voltage\"\:127.34 \"Current\"\:17.22 \"PF\"\:23.12 \"Consumption\":231.34 ");
	/* "Voltage":127.34 "Current":17.2 "PF":23.12 "Consumption":231.34 */
	
	printString(cad);
	//printString(buffer);
	
	while (1)
	{
		//do nothing while waiting for interrupts to trigger
		//printString(cad);
	}
}

