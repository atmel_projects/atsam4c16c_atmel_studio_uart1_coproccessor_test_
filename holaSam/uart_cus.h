/*
 * uart_cus.h
 *
 * Created: 04/09/2017 09:05:00 a.m.
 *  Author: hydro
 */ 




#ifndef UART_CUS_H_
#define UART_CUS_H_

#include "sam.h"

void UART_Init();
void transmitByte(uint8_t data);
void printString(const char myString[]);
void UART0_Handler(void);

#endif /* UART_CUS_H_ */
